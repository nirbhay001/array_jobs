const data = require('./1-arrays-jobs.cjs');

function updatedSalary(data) {
    let result = data.reduce((previous, current) => {
        current['corrected_salary'] = Number(current.salary.replace("$", "") * 10000);
        previous.push(current);
        return previous;
    }, [])
    return result;
}
console.log(updatedSalary(data));

