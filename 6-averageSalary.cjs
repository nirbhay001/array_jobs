const data = require('./1-arrays-jobs.cjs');

function averageSalary(data) {
    let result = data.reduce((previous, current) => {
        let place = current.location;
        let salaryies = Number(current.salary.replace("$", ""));

        if (previous[place] === undefined) {
            previous[place] = {};
            previous[place]['sum'] = Number((salaryies).toFixed(2));
            previous[place]['count'] = 1;
        }
        else {
            previous[place].sum += Number((salaryies).toFixed(2));
            previous[place].count += 1;
        }
        return previous;
    }, {})
    let returnData = Object.entries(result).reduce((prev, curr) => {
        curr[1]['Average'] = (Number(curr[1].sum) / curr[1].count).toFixed(2);
        prev.push(curr);
        return prev;
    }, []);

    return returnData;

}
console.log(Object.fromEntries((averageSalary(data))));
