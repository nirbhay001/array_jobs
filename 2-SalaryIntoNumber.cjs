const data = require('./1-arrays-jobs.cjs');
function salaryNumber(data) {
    let result = data.map((item) => {
        item.salary = +(item.salary.replace("$", ""))
        return item;
    })
    return result;
}
console.log(salaryNumber(data));