const data = require('./1-arrays-jobs.cjs');

function sumSalary(data) {
    let result = data.reduce((previous, current) => {
        let salaryData = Number(current.salary.replace("$", ''));
        return (previous + salaryData);
    }, 0)
    return result;
}
console.log(sumSalary(data).toFixed(2));