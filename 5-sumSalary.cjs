const data = require('./1-arrays-jobs.cjs');

function sumSalary(data) {
    let result = data.reduce((previous, current) => {
        let place = current.location;
        let salaryies = Number(current.salary.replace("$", ""));
        if (previous[place] === undefined) {
            previous[place] = {};
            previous[place]['sum'] = Number(salaryies);
        }
        else {
            previous[place].sum += Number(salaryies);
        }
        return previous;
    }, {})
    return result;
}
console.log(sumSalary(data));